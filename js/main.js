import Tasks from "./models/tasks.js";
import { callApi } from "./utils/callapi.js";

//Hàm getEleByID
const getEle = (id) => document.getElementById(id);

//ShowlistTask
const ShowlistTask = () => {
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`Tasks`, "GET", null)
    .then((result) => {
      //tắt loader
      getEle("loader").style.display = "none";
      console.log(result.data);
      renderTaskTable(result.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
ShowlistTask();

//Hàm renderTaskTable
const renderTaskTable = (arr) => {
  let contentTable = "";
  arr.forEach((item) => {
    contentTable += `
        <ul class="d-flex flex-row justify-content-between p-1" style="border-radius:7px; border:1px solid #aaaaaa; margin-bottom:10px; color:#aaaaaa; background:#ffffff;">${item.content}
            <div> 
                <button onclick="deleteTasks(${item.id})" class="btn p-0" style="border:none; background:none;">
                    <i class="fas fa-trash" style="color:#aaaaaa;"></i>
                </button>
                <button onclick = "handledCompleteTasks(${item.id})" class="btn p-0" style="border:none; background:none;">
                    <i class="far fa-check-circle" style="color:#aaaaaa;"></i>
                </button>
            </div>
        </ul>
    `;
  });
  getEle("todo").innerHTML = contentTable;
};

//Hàm dom getInfo từ user nhập vào
const getInfo = () => {
  let _id;
  const _content = getEle("newTask").value;
  console.log(_content);
  //tạo đối tượng task từ lớp đối tượng Tasks
  const task = new Tasks(_id, _content);
  return task;
};

//Thêm Task
getEle("addItem").addEventListener("click", (event) => {
  //Enter
  event.preventDefault();
  const task = getInfo();
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`Tasks`, "POST", task)
    .then(() => {
      //tắt loader
      getEle("loader").style.display = "none";
      ShowlistTask();
      //reset input
      getEle("newTask").value = "";
    })
    .catch((err) => {
      console.log(err);
    });
});

//Hàm deleteTasks
const deleteTasks = (id) => {
  callApi(`Tasks/${id}`, "DELETE", null)
    .then(() => {
      ShowlistTask();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.deleteTasks = deleteTasks;

//Hàm renderCompletedTasksTable
const renderCompletedTasksTable = (arr) => {
  let contentCompletedTable = "";
  arr.forEach((item) => {
    contentCompletedTable += `
          <ul class="d-flex flex-row justify-content-between p-1" style="border-radius:7px; border:1px solid #aaaaaa; margin-bottom:10px; color:#28a745; background:#ffffff;">${item.content}
              <div> 
                  <button onclick="deleteTasks(${item.id})" class="btn p-0" style="border:none; background:none;">
                      <i class="fas fa-trash" style="color:#aaaaaa;"></i>
                  </button>
                  <button onclick = "handledCompleteTasks(${item.id})" class="btn p-0" style="border:none; background:none;">
                      <i class="far fa-check-circle" style="color:#28a745"></i>
                  </button>
              </div>
          </ul>
      `;
  });
  getEle("completed").innerHTML = contentCompletedTable;
};

let arrTasksDone = [];
//Hàm handledCompleteTasks
const handledCompleteTasks = (id) => {
  callApi(`Tasks/${id}`, "GET", null)
    .then((result) => {
      arrTasksDone.push(result.data);
      console.log(arrTasksDone);
      renderCompletedTasksTable(arrTasksDone);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.handledCompleteTasks = handledCompleteTasks;

//Hàm sortTasks
const sortTasks = (data) => {
  let sortAZ = data.sort((task2, task1) => {
    let task2Name = task2.content.toLowerCase();
    let task1Name = task1.content.toLocaleLowerCase();
    if (task2Name > task1Name) {
      return 1; //Giữ Nguyên
    }
    if (task2Name < task1Name) {
      return -1; //Đảo vị trí
    }
    return 1;
  });
  return sortAZ;
};
//Hàm Sort ASC
getEle("two").addEventListener("click", () => {
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`Tasks`, "GET", null)
    .then((result) => {
      //tắt loader
      getEle("loader").style.display = "none";
      const sortAZ = sortTasks(result.data);
      renderTaskTable(sortAZ);
    })
    .catch((err) => {
      console.log(err);
    });
});
//Hàm Sort DSC
getEle("three").addEventListener("click", () => {
  //bật loader
  getEle("loader").style.display = "block";
  callApi(`Tasks`, "GET", null)
    .then((result) => {
      //tắt loader
      getEle("loader").style.display = "none";
      let sortZA = sortTasks(result.data);
      sortZA.reverse();
      renderTaskTable(sortZA);
    })
    .catch((err) => {
      console.log(err);
    });
});
