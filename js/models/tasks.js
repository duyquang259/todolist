import { callApi } from "../utils/callapi.js";

export default class Tasks {
  constructor(_id, _content) {
    this.id = _id;
    this.content = _content;
  }
}

// export default class ListCompletedTasks {
//     constructor(_id,_content){
//         this.id = _id;
//         this.content
//     }
// }
